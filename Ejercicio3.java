package modulo1;

public class Ejercicio3 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Tecla de escape \t\t Significado");
		System.out.println("\n\\n \t\t\t\t significa nueva linea");
		System.out.println("\\t \t\t\t\t significa un tab de espacio");
		System.out.println("\\\" \t\t\t\t es para poner \"(comillas dobles) dentro del texto, por ejemplo \"belencita\"");
		System.out.println("\\\\ \t\t\t\t se utiliza para escribir la \\ dentro del texto, por ejemplo \\algo\\ ");
		System.out.println("\\\'  \t\t\t\t se utiliza para las \'(comillas simples), para escribir por ejemplo \'princesita\'");
	}

}
